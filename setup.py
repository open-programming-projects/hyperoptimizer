from setuptools import setup, find_packages

setup(name='hyperoptimizer',
      version='1.0.0',
      author='David Zanger',
      author_email='david.zanger@dlr.de',
      packages=find_packages(),
      long_description=open('README.md').read())
