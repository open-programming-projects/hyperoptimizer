from abc import ABC, abstractmethod
from typing import Any, Dict, List, Optional

from dataclasses import dataclass


@dataclass
class OptimizationSolution:
    objective_value: float
    stats: Optional[Dict[str, Any]]
    values: Optional[Dict[Any, Any]]
    error: Optional[str]


class Optimizer(ABC):

    def __init__(self) -> None:
        self.objective_value = None

    @abstractmethod
    def variable(self) -> Any:
        pass

    @abstractmethod
    def objective_function(self) -> None:
        pass

    @abstractmethod
    def subject_to(self) -> None:
        pass

    @abstractmethod
    def set_parameters(self, value: Any, parameter: Any) -> Any:
        pass

    @abstractmethod
    def set_solver_parameters(self, parameter_settings: Any) -> Any:
        pass

    @abstractmethod
    def set_initial_guess(self, variables: Any, initial_guess: Any) -> Any:
        pass

    @abstractmethod
    def solve(self) -> OptimizationSolution:
        pass
