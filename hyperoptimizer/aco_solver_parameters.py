from dataclasses import dataclass
from hyperoptimizer.solver_parameter import SolverParameters, Int, Float, Category
import numpy as np
from hyperoptimizer import ParameterFactoryObject


@dataclass
class AcoSolverParameters(SolverParameters):
    pheromone_evaporation_ratio: Float  # Pheromone evaporation value
    pheromone_start_value: Float
    beta: Float  # Visibility enhancement value
    q0: Float  # Transition value
    n_runs_aco: Int  # Number of ACO runs
    n_ants_per_run: Int  # Number of ants per optimization run


ParameterFactoryObject.register_parameter_class('aco', AcoSolverParameters)
