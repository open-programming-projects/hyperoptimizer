from dataclasses import dataclass
from hyperoptimizer.solver_parameter import SolverParameters, Int, Float, Category
import numpy as np
from hyperoptimizer import ParameterFactoryObject


@dataclass
class SpecificSolverParameters2(SolverParameters):
    para_1: Int = Int(0, 10, step=1)
    para_2: Float = Float(0, 10, log=True)
    para_3: Category = Category(['a', 'b', 'c'])


ParameterFactoryObject.register_parameter_class('solver_2',
                                                SpecificSolverParameters2)
