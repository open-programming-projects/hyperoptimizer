from dataclasses import dataclass, field
from typing import List
import copy


# From: https://stackoverflow.com/questions/52063759/passing-default-list-argument-to-dataclasses
def default_field(obj):
    return field(default_factory=lambda: copy.deepcopy(obj))


@dataclass
class OptunaParameters:
    n_trials: int = 100
    solvers: List[str] = default_field(['ipopt'])
    print_level: int = 2