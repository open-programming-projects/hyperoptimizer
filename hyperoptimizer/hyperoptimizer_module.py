from hyperoptimizer.optimizer_factory import OptimizerFactory
from hyperoptimizer.hyperoptimizer_factory import HyperoptimizerFactory
from typing import Any, List
from hyperoptimizer.optimizer import Optimizer
from hyperoptimizer.solver_parameter_factory import SolverParameterFactory


class Hyperoptimizer(Optimizer):

    def __init__(self) -> None:
        self.solver: List[str] = ['']
        self.hyperoptimizer = HyperoptimizerFactory.create_hyperoptimizer()
        self.optimization_problem = None

    def variable(self) -> Any:
        pass

    def minimize(self) -> None:
        pass

    def subject_to(self) -> None:
        pass

    def set_parameters(self) -> Any:
        pass

    def solve(self) -> Any:
        results = []
        for solver in self.solver:
            parameters = SolverParameterFactory.get_hyperparameter(solver)
            optimizer = OptimizerFactory.create_optimizer(
                solver, self.optimization_problem)
            results_from_solver = self.hyperoptimizer.solve(
                optimizer, parameters)
            results.append(results_from_solver)
        best_results = self.compare_results(results)
        return best_results

    def compare_results(self, results: Any) -> Any:
        pass
