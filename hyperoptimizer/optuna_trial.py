from typing import Optional
import optuna
from optuna.study import ObjectiveFuncType


def objective(trial):
    classifier_name = trial.suggest_categorical("classifier",
                                                ["solver_1", "solver_2"])
    if classifier_name == "solver_1":
        parameter_1 = trial.suggest_float("parameter_1", 1e-10, 1e2, log=True)
        objective = (parameter_1 - 2)**2
        time.perf_counter()
        x = Variable()

    elif classifier_name == "solver_2":
        parameter_2 = trial.suggest_int("parameter_2", 1, 32, step=1)
        objective = (parameter_2 - 2)**2
    return objective


study = optuna.create_study()
study.optimize(objective, n_trials=100)

best_params = study.best_params
found_x = best_params["parameter_1"]
print("Found x: {}, (x - 2)^2: {}".format(found_x, (found_x - 2)**2))

study.best_params

study.best_value

study.best_trial

study.trials

study.optimize(objective, n_trials=100)

best_params = study.best_params
found_x = best_params["parameter_1"]
print("Found x: {}, (x - 2)^2: {}".format(found_x, (found_x - 2)**2))
