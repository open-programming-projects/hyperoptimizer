from typing import Any
from hyperoptimizer.optimizer import Optimizer


class OptimizerFactory:

    @staticmethod
    def create_optimizer(solver: str, problem_formulation: Any) -> Optimizer:
        if solver == 'ipopt':
            pass
        elif solver == 'xxx':
            problem_formulation = (
                OptimizerFactory.reallocate_problem_from_casadi_to_xxx(
                    problem_formulation))
        else:
            raise ValueError(f'Solver type {solver} is not known.')

        return problem_formulation

    @staticmethod
    def reallocate_problem_from_casadi_to_xxx(
            problem_formulation: Any) -> Optimizer:
        pass