from dataclasses import dataclass
from typing import Any, List, Optional


@dataclass
class ParameterType:
    pass


@dataclass
class Category(ParameterType):
    elements: List[Any]


@dataclass
class Int(ParameterType):
    lower: Optional[int]
    upper: Optional[int] = None
    log: bool = False
    step: int = 1

    def __post_init__(self):
        if self.upper is None:
            self.upper = self.lower


@dataclass
class Float(ParameterType):
    lower: Optional[float]
    upper: Optional[float] = None
    log: bool = False
    step: Optional[float] = None

    def __post_init__(self):
        if self.upper is None:
            self.upper = self.lower


@dataclass
class SolverParameters:
    pass