from typing import Any
from hyperoptimizer.solver_parameter import SolverParameters


class ParameterFactory:
    """Factory to create and register new paramater classes.

    The class idea comes from: https://realpython.com/factory-method-python/#factory-method-as-an-object-factory
    """

    def __init__(self):
        self._parameter_class_dict = {}

    def register_parameter_class(self, key: str, parameter_class: Any) -> None:
        self._parameter_class_dict[key] = parameter_class

    def get_parameters(self, key: str, **kwargs) -> SolverParameters:
        parameter_class = self._parameter_class_dict.get(key)
        if not parameter_class:
            raise ValueError(f'Solver {key} is not registered.')
        return parameter_class(**kwargs)


factory = ParameterFactory()