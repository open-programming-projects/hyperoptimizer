from typing import Any, Callable, Dict, List
from hyperoptimizer.optimizer import Optimizer
import optuna
import hyperoptimizer.solver_parameter as sp
from dataclasses import fields
from hyperoptimizer import ParameterFactoryObject, OptunaParameters
import logging

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)s %(levelname)s:%(message)s')
logger = logging.getLogger(__name__)


class OptunaHyperoptimizer():

    def __init__(
        self,
        optimization_problem: Optimizer,
        optuna_parameters: OptunaParameters = OptunaParameters()
    ) -> None:
        self._optimization_problem: Optimizer = optimization_problem
        self.parameters: OptunaParameters = optuna_parameters
        self.current_solver_parameters: Dict[str, Any] = {}

    def set_solver_parameters(self, parameters: OptunaParameters) -> None:
        self.parameters = parameters

    def hyperoptimize(self) -> Any:
        objective = self._create_objective_function()
        study = optuna.create_study()
        study.optimize(objective, n_trials=self.parameters.n_trials)
        if self.parameters.print_level >= 1:
            print(f'Best parameter configuration: {study.best_params}')
        return self._optimization_problem

    def _create_objective_function(self) -> Callable[[Any], float]:

        def objective(trial) -> float:
            classifier_name = trial.suggest_categorical('solver_name',
                                                        self.parameters.solvers)
            if classifier_name == "ipopt":
                parameter_list = self._create_parameter_list(
                    trial, classifier_name)
            else:
                raise ValueError(f'Solver {classifier_name} not known.')
            parameter_list['solver_name'] = classifier_name
            self._optimization_problem.set_solver_parameters(parameter_list)
            solution = self._optimization_problem.solve()
            objective_value = solution.objective_value
            if solution.error:
                logger.warning(solution.error)
            return objective_value

        return objective

    @staticmethod
    def _create_parameter_list(trial: optuna.Trial,
                               solver: str) -> Dict[str, Any]:
        parameters = ParameterFactoryObject.get_parameters(solver)
        parameter_list = OptunaHyperoptimizer._map_parameters_to_trial(
            trial, parameters)
        return parameter_list

    @staticmethod
    def _map_parameters_to_trial(
            trial: optuna.Trial,
            parameter_object: sp.SolverParameters) -> Dict[str, Any]:
        parameter_list = {}
        for parameter in fields(parameter_object):
            value = getattr(parameter_object, parameter.name)
            parameter_list[parameter.name] = (
                OptunaHyperoptimizer._suggest_parameter(trial, parameter.name,
                                                        value))
        return parameter_list

    @staticmethod
    def _suggest_parameter(trial: optuna.Trial, name: str,
                           parameter: sp.ParameterType) -> Any:
        suggested_parameter = None
        if isinstance(parameter, sp.Category):
            suggested_parameter = trial.suggest_categorical(
                name, parameter.elements)
        elif isinstance(parameter, sp.Int):
            if parameter.lower is not None:
                suggested_parameter = trial.suggest_int(name,
                                                        parameter.lower,
                                                        parameter.upper,
                                                        log=parameter.log,
                                                        step=parameter.step)
        elif isinstance(parameter, sp.Float):
            if parameter.lower is not None:
                suggested_parameter = trial.suggest_float(name,
                                                          parameter.lower,
                                                          parameter.upper,
                                                          log=parameter.log,
                                                          step=parameter.step)
        return suggested_parameter
