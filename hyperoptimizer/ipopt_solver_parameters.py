from dataclasses import dataclass
from hyperoptimizer.solver_parameter import SolverParameters, Int, Float, Category
import numpy as np
from hyperoptimizer import ParameterFactoryObject


@dataclass
class IpoptSolverParameters(SolverParameters):
    # Options mentioned in: https://drops.dagstuhl.de/volltexte/2009/2089/pdf/09061.WaechterAndreas.Paper.2089.pdf
    # and: https://coin-or.github.io/Ipopt/OPTIONS.html
    max_iter: Int = Int(10, 3000)
    hessian_approximation: Category = Category(['limited-memory', 'exact'])
    limited_memory_update_type: Category = Category(['bfgs'])
    # Tolerance
    # tol: Float = Float(None)
    print_level: Int = Int(1)
    # However, switching mehrotra_algorithm to yes might make Ipopt perform much better for linear programs (LPs) or convex quadratic programs (QPs) by choosing some default option differently.
    mehrotra_algorithm: Category = Category(['yes', 'no'])
    # Might work well in nonlinearcircumstances; for many problems, this adaptive barrier strategy seems toreduce the number of iterations, but at a somewhat higher computationalcosts per iteration.
    # mu_strategy = Category(['monotone', 'adaptive'])


# [16:16] Eugen Nuss
# ipopt_bfgs =  {'jit': True,                                              'print_time': False,                                              'expand': False,                                              'ipopt': {'hessian_approximation': 'limited-memory',                                                        'limited_memory_update_type': 'bfgs',                                                        'max_iter': 100,                                                        'print_level': 1}}

# [16:16] Eugen Nuss
#         opti.solver('ipopt', ipopt_bfgs)

ParameterFactoryObject.register_parameter_class('ipopt', IpoptSolverParameters)
