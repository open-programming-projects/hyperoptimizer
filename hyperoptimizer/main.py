from dataclasses import fields
import hyperoptimizer.solver_parameter as sp
from hyperoptimizer import ParameterFactoryObject, CasadiOptimizer, OptunaHyperoptimizer
import casadi
from dataclasses import dataclass


def main():
    # Example from Casadi.
    opti = casadi.Opti()

    x = opti.variable()
    y = opti.variable()

    opti.minimize((y - x**2)**2)
    opti.subject_to(x**2 + y**2 == 1)
    opti.subject_to(x + y >= 1)

    opti.solver('ipopt')

    sol = opti.solve()
    print(sol)

    # Example using this optimizer.
    opti = CasadiOptimizer()

    x = opti.variable()
    y = opti.variable()

    def function(a, b):
        return (b - a**2)**2

    obj = function(x, y)
    opti.objective_function(obj)
    opti.subject_to(x**2 + y**2 == 1)
    opti.subject_to(x + y >= 1)

    opti.set_solver_parameters({'solver_name': 'ipopt'})

    sol = opti.solve()
    print(sol)

    hyopti = OptunaHyperoptimizer(opti)
    hyopti.hyperoptimize()


if __name__ == "__main__":
    main()