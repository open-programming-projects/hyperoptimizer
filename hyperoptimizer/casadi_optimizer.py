from typing import Any, Dict, Optional
from hyperoptimizer.optimizer import Optimizer, OptimizationSolution
import casadi
import warnings
import numpy as np


class CasadiOptimizer(Optimizer):

    def __init__(self) -> None:
        self.optimizer = casadi.Opti()
        self.objective = None
        self.parameters: Dict[str, Any] = {}

    def variable(self) -> Any:
        return self.optimizer.variable()

    def objective_function(self, objective: Any) -> None:
        self.objective = objective
        self.optimizer.minimize(objective)

    def subject_to(self, condition: Any) -> None:
        self.optimizer.subject_to(condition)

    def set_initial_guess(self, variables: Any, initial_guess: Any) -> Any:
        self.optimizer.set_initial(variables, initial_guess)

    def set_parameters(self,
                       value: Any,
                       parameter: Optional[Any] = None) -> Any:
        if parameter is None:
            parameter = self.optimizer.parameter()
        self.optimizer.set_value(parameter, value)
        return parameter

    def set_solver_parameters(self, parameter_settings: Dict[str, Any]) -> None:
        solver_key = 'solver_name'
        solver_name = ''
        if solver_key in parameter_settings.keys():
            solver_name = parameter_settings[solver_key]
        else:
            ValueError('Solver name is not specified.')

        # Extract everything except solver name.
        parameters = {
            k: parameter_settings[k]
            for k in parameter_settings.keys()
            if k != solver_key
        }

        self.optimizer.solver(solver_name, {}, parameters)
        self.parameters = parameter_settings

    def solve(self) -> OptimizationSolution:
        casadi_solution = None
        error = ''
        try:
            casadi_solution = self.optimizer.solve()
        except RuntimeError as e:
            if 'Maximum_Iterations_Exceeded' in str(e):
                warnings.warn('Maximum number of iterations reached.')
            else:
                error = f'Error occured: {str(e)} using the following parameters: {self.parameters}'
        objective_value = np.inf
        stats = None
        if casadi_solution:
            objective_value = casadi_solution.value(self.objective)
            stats = casadi_solution.stats()
        solution = OptimizationSolution(objective_value, stats, None, error)
        return solution
