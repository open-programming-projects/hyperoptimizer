# Agenda

## Sprint #1 (2021-03-15)

Sprint Planning 1:
Questions:

- Are there any interesting conferences for me?
  - You can also write authors of papers and make Zoom calls.
  - IFAC (different conferences): CDC, IFAC World, CCTA
- What about the price recommendation?
- Tell about Enums.
- What about AutoML?
- 

Suggested Requirements (IDs):
Requirements To-Do (IDs):

Sprint Planning 2:
How to complete the requirements?

Sprint Review:
Which backlog items (requirements) or tasks where accomplished?

Which items could not be accomplished? Were did problems rise up?

Retroperspective:
What was good?

What can we do better?
