# Optimization

Links to different Python MILP optimization libraries can be found here:

- [Link](https://stackoverflow.com/questions/26305704/python-mixed-integer-linear-programming) from stackoverflow.
- [Link](https://www.researchgate.net/post/Can-you-suggest-best-solver-for-the-mixed-integer-nonlinear-programming) from researchgate.
- May combine Pyomo with BONMIN as MINLP solver (see [Link](https://stackoverflow.com/questions/42391945/solving-minlp-with-pyomo-and-bonmin))
- May try [MAiNGO](https://git.rwth-aachen.de/avt.svt/public/maingo).
- May try [GEKKO](https://gekko.readthedocs.io/en/latest/).

## Hyper Parameter Tuning

- Some tool suggestions for hyperparameter tuning can be found here: [Link](https://neptune.ai/blog/hyperparameter-tuning-in-python-a-complete-guide-2020)
- Use Optuna over Hyperopt as explained [here](https://neptune.ai/blog/optuna-vs-hyperopt).
- Spearmint can be used as Bayesian Optimizer (see [GitHub](https://github.com/HIPS/Spearmint))
- pyGPGO is a Bayesian optimization library for Python (see [Link](https://pygpgo.readthedocs.io/en/latest/))
- Nevergrad is a library of different hyperoptimizers for Python: [Link](https://engineering.fb.com/2018/12/20/ai-research/nevergrad/)
- [Ax](https://ax.dev/docs/why-ax.html) this is more high-level than BoTorch.
- [BoTorch](https://botorch.org/) is Bayesian hyperoptimizer with GPU computation.

> I will prefer BoTorch because it has the most contributors.
