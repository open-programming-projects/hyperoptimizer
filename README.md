# Hyperoptimizer

## Installation

By installing the project as a package you can import the modules within the package in other Python modules by using:

> `import hyperoptimizer`

Install in "develop" or "editable" mode (change directory to the root folder with the setup.py in it and execute via the anaconda prompt for example):

> `pip install -e .` (recommended)

or:

> `python setup.py develop`

It puts a link (actually *.pth files) into the python installation to your code, so that your package is installed, but any changes will immediately take effect.

In order to test the package execute

> `pytest --pyargs hyperoptimizer`

## Rationale Project Structure

- The `tests` folder needs a `__init__.py` file, otherwise VS Code can not execute the tests.
